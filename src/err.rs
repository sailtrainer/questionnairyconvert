use thiserror::Error;

#[derive(Error, Debug)]
pub enum AppError {
    #[error("I/O error")]
    IoError(#[from] std::io::Error),

    #[error("Parsing error")]
    XmlError(#[from] xml::reader::Error),

    #[error("Serialization error")]
    JsonError(#[from] serde_json::Error),
}
