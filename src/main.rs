use crate::err::AppError;
use serde::{Deserialize, Serialize};
use serde_json::ser::to_string_pretty;
use std::fs::File;
use uuid::Uuid;
use xml::attribute::OwnedAttribute;
use xml::reader::XmlEvent;

mod err;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Questionnaire {
    topics: Vec<Topic>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Topic {
    topic_id: Uuid,
    legacy_id: String,
    name: String,
    questions: Vec<Question>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Question {
    question_id: Uuid,
    legacy_id: String,
    reference: String,
    text: String,
    correct: String,
    incorrect: Vec<String>,
}

fn main() -> Result<(), AppError> {
    let input = File::open("input.xml")?;
    let parser = xml::reader::EventReader::new(input);

    let mut questionnaire = Questionnaire { topics: vec![] };

    let mut current_topic: Option<Topic> = None;
    let mut current_question: Option<Question> = None;
    let mut current_incorrect = String::new();

    let mut xml_path = Vec::<String>::new();

    for event in parser {
        match event? {
            XmlEvent::StartDocument {
                version: _,
                encoding: _,
                standalone: _,
            } => {
                println!("StartDocument")
            }
            XmlEvent::EndDocument => {
                println!("EndDocument")
            }
            XmlEvent::ProcessingInstruction { name: _, data: _ } => {
                println!("ProcessingInstruction")
            }
            XmlEvent::StartElement {
                name,
                attributes,
                namespace: _,
            } => {
                xml_path.push(name.local_name);
                if is_topic_path(&xml_path) {
                    current_topic = Some(Topic {
                        topic_id: Uuid::new_v4(),
                        legacy_id: get_attribute(&attributes, "id").unwrap_or("none".to_string()),
                        name: get_attribute(&attributes, "name").unwrap_or("unnamed".to_string()),
                        questions: vec![],
                    });
                } else if is_question_path(&xml_path) {
                    current_question = Some(Question {
                        question_id: Uuid::new_v4(),
                        legacy_id: get_attribute(&attributes, "id").unwrap_or("none".to_string()),
                        reference: get_attribute(&attributes, "reference")
                            .unwrap_or("none".to_string()),
                        text: "".to_string(),
                        correct: "".to_string(),
                        incorrect: vec![],
                    });
                } else if is_incorrect_path(&xml_path) {
                    current_incorrect = String::new();
                }
            }
            XmlEvent::EndElement { name: _ } => {
                if is_topic_path(&xml_path) {
                    if let Some(ref topic) = current_topic {
                        questionnaire.topics.push(topic.clone())
                    }
                    current_topic = None;
                } else if is_question_path(&xml_path) {
                    if let Some(question) = current_question {
                        if let Some(ref mut topic) = current_topic {
                            topic.questions.push(question);
                        }
                    }
                    current_question = None;
                } else if is_incorrect_path(&xml_path) {
                    if let Some(ref mut question) = current_question {
                        question.incorrect.push(current_incorrect);
                        current_incorrect = String::new();
                    }
                }
                xml_path.pop();
            }
            XmlEvent::CData(data) => {
                if is_text_path(&xml_path) {
                    if is_text_path(&xml_path) {
                        if let Some(ref mut question) = current_question {
                            question.text += &data;
                        }
                    } else if is_correct_path(&xml_path) {
                        if let Some(ref mut question) = current_question {
                            question.correct += &data;
                        }
                    } else if is_incorrect_path(&xml_path) {
                        current_incorrect += &data;
                    }
                }
            }
            XmlEvent::Comment(comment) => println!("Comment: {}", comment),
            XmlEvent::Characters(characters) => {
                if is_text_path(&xml_path) {
                    if let Some(ref mut question) = current_question {
                        question.text += &characters;
                    }
                } else if is_correct_path(&xml_path) {
                    if let Some(ref mut question) = current_question {
                        question.correct += &characters;
                    }
                } else if is_incorrect_path(&xml_path) {
                    current_incorrect += &characters;
                }
            }
            XmlEvent::Whitespace(whitespace) => {
                if is_text_path(&xml_path) {
                    if let Some(ref mut question) = current_question {
                        question.text += &whitespace;
                    }
                } else if is_correct_path(&xml_path) {
                    if let Some(ref mut question) = current_question {
                        question.correct += &whitespace;
                    }
                } else if is_incorrect_path(&xml_path) {
                    current_incorrect += &whitespace;
                }
            }
        }
    }

    std::fs::write("output.json", serde_json::to_string_pretty(&questionnaire)?)?;

    let converted = to_string_pretty(&questionnaire)?;
    println!("{}", converted);

    Ok(())
}

fn is_topic_path(xml_path: &Vec<String>) -> bool {
    path_is(xml_path, "topic", 2)
}

fn is_question_path(xml_path: &Vec<String>) -> bool {
    path_is(xml_path, "question", 3)
}

fn is_text_path(xml_path: &Vec<String>) -> bool {
    path_is(xml_path, "text", 4)
}

fn is_correct_path(xml_path: &Vec<String>) -> bool {
    path_is(xml_path, "correct", 4) || path_is(xml_path, "answer", 4)
}

fn is_incorrect_path(xml_path: &Vec<String>) -> bool {
    path_is(xml_path, "incorrect", 4)
}

fn path_is(xml_path: &Vec<String>, last_tag: &str, depth: usize) -> bool {
    xml_path.len() == depth && xml_path.last().cloned() == Some(last_tag.to_string())
}

fn get_attribute(attributes: &Vec<OwnedAttribute>, name: &str) -> Option<String> {
    attributes
        .iter()
        .filter(|a| a.name.local_name == name.to_string())
        .next()
        .map(|a| a.value.clone())
}
